const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth.js")

// Check email
module.exports.checkEmailExists = (req, res) => {
	return User.find({email: req.body.email}).then(result => {

		// The "find" method return a record if a match is found
		if(result.length > 0) {
			return res.send (true);

		// No duplicate email found
		// The user is not yet registered in the database
		} else {
			return res.send (false);
		}
	})
	.catch(error => res.send (error))
}

// User registration
module.exports.registerUser = (req, res) => {
	let newUser = new User({
		email: req.body.email,
		password: bcrypt.hashSync(req.body.password, 10),
		mobileNo: req.body.mobileNo
	})

	// Save the created object to our database
	return newUser.save().then(user => {
		console.log(user);
		res.send(true)
	})
	.catch(error => {
		console.log(error);
		res.send(false);
	})
}

// User authentication
module.exports.loginUser = (req, res) => {

	return User.findOne({email: req.body.email}).then(result => {

		if(result == null) {
			return res.send({message: "No user found"})

		} else {

			const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);

			if (isPasswordCorrect) {

				return res.send({accessToken: auth.createAccessToken(result)});
			
			} else {
				return res.send(false)
			}
		}
	})
}

// Retrieve user details
module.exports.getProfile = (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	console.log(userData);

	return User.findById(userData.id).then(result => {

		result.password = "password encrypted";

		return res.send(result);

	})

}

// Create order of non-admin user
module.exports.order = async (req, res) => {

	//Get the token in header and decode
	const userData = auth.decode(req.headers.authorization);

	console.log(userData);

	if(userData.isAdmin === false) {

		//This will retrieve the productName in product list using the
		// productId as reference from the request body
		let product = await Product.findById(req.params.productId).then(result => result); 

		let data = {

			// User ID and email will be retrieved from the payload/token
			userId: userData.id,
			email: userData.email,
			productId: product.id.toString(),
			quantity: req.body.quantity,
			subTotal: product.price * req.body.quantity,
			productName: product.name
		}

		console.log(data);


		let isUserUpdated = await User.findById(data.userId).then(user => {

			console.log(user);

			user.orders.push({
				products: [
					{
						productId: data.productId,
						productName: data.productName,
						quantity: data.quantity,
						subTotal: data.subTotal
					}
				],
				totalAmount: data.subTotal
			});

			// Saves the updated user information in the database
			return user.save().then(result => {
				console.log(result);
				return true;
			})
			.catch(error => {
				console.log(error);
				return false;
			});
		});
		console.log(isUserUpdated);


		// Retrieve orderId in user's order array 
		let orderSavedToUser  = await User.findById(data.userId).then(result => result.orders); 
		console.log(orderSavedToUser);

		let lastElementIndex = orderSavedToUser.length-1;
		let currentOrderId = orderSavedToUser[lastElementIndex]._id

		console.log(currentOrderId);

		let isProductUpdated = await Product.findById(data.productId)
		.then(product => {

			// Adds the orderId in the course's enrollees array
			product.orders.push({
				orderId: currentOrderId.toString()
			})

			// Saves the updated product information in the database
			return product.save().then(result => {
				console.log(result);
				return true;
			})
			.catch(error => {
				console.log(error);
				return false;
			});
		});
		console.log(isProductUpdated);


		// Condition that will check if the user and course documents have been updated

		// User order successful
		if(isUserUpdated && isProductUpdated) {
			return res.send(true);

		// User order failed
		} else {
			return res.send(false);
		}
	} else {
		return res.send(false);
	}
}

// Set user as admin
module.exports.setAdmin = (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if (userData.isAdmin) {

		let makeAdmin = {
			isAdmin: true
		}

		return User.findByIdAndUpdate(req.params.userId, makeAdmin, {new:true}).then(result => {
			console.log(result);
			res.send(result)
		})
		.catch(error => {
			console.log(error);
			res.send(false);
		})
	} else {
		return res.send(false);
	}
}

// Retrieve user orders
module.exports.getOrder = (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	console.log(userData);

	return User.findById(userData.id).then(result => {

		let userOrders = result.orders;

		return res.send(userOrders);

	})

}

// Retrieve all orders (admin)
module.exports.getAllOrder = async (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if (userData.isAdmin) {

		let users = await User.find({isAdmin: false}).then(users => users)
		
		console.log(users)	

		dataToReturn = []

		for (i = 0; i < users.length; i++) {
			const userId = users[i].id;
			const orders = users[i].orders
			console.log(userId);
			console.log(orders);
		
			userOrderData = {
				user: userId,
				orderHistory: orders
			}

			dataToReturn.push(userOrderData);

		}

		return res.send(dataToReturn)

		/*const orders = users.map(user => user.orders).flat();

		return res.send(orders)*/
		
	} else {
		return res.send(false);
	}
}

// Create addToCart of non-admin user
module.exports.addToCart = async (req, res) => {

	//Get the token in header and decode
	const userData = auth.decode(req.headers.authorization);

	console.log(userData);

	if(userData.isAdmin === false) {

		//This will retrieve the productName in product list using the
		// productId as reference from the request body
		let product = await Product.findById(req.params.productId).then(result => result); 

		let data = {

			// User ID and email will be retrieved from the payload/token
			userId: userData.id,
			email: userData.email,
			productId: product.id.toString(),
			price: product.price,
			quantity: req.body.quantity,
			subTotal: product.price * req.body.quantity,
			productName: product.name
		}

		console.log(data);


		let isUserUpdated = await User.findById(data.userId).then(user => {

			console.log(user);

			user.carts.push({
				
				productId: data.productId,
				productName: data.productName,
				price: data.price,
				quantity: data.quantity,
				subTotal: data.subTotal
	
			});

			// Saves the updated user information in the database
			return user.save().then(result => {
				console.log(result);
				return res.send("Added to cart!");
			})
			.catch(error => {
				console.log(error);
				return false;
			});
		});

	} else {
		return res.send("You don't have permission to process order");
	}
}

// Retrieve user cart
module.exports.getCart = (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	console.log(userData);



	return User.findById(userData.id).then(result => {

		let cartItems = result.carts;
		console.log(cartItems)

		let total = 0;


		for (let i = 0; i < cartItems.length; i++) {
		    total += cartItems[i].subTotal;
		}

		dataToReturn = []
		dataToReturn.push(cartItems);
		dataToReturn.push({totalAmount: total});

		return res.send(dataToReturn)

	})
}
