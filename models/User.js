const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	email: {
		type: String,
		required: [true, "Email is required"],
		unique: true // If duplicate will return error
	},
	password: {
		type: String,
		required: [true, "Password  is required"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	mobileNo: {
		type: String,
		required: [true, "Mobile number is required"],
		unique: true // If duplicate will return error
	},
	orders: [

		{
			products: [
			    {
			    	productId: {
			    		type: String
			    	},
			    	productName: {
			    		type: String
			    	},
				    quantity: {
				        type: Number
			      	},
				    subTotal: {
				        type: Number
			      	}
			    }
			],
			totalAmount: {
				type: Number
			},
			PurchasedOn: {
				type: Date,
				default: new Date()
			}
		}
	],
	carts: [

    	{
	    	productId: {
	    		type: String
	    	},
	    	productName: {
	    		type: String
	    	},
	    	price: {
	    		type: Number
	    	},
		    quantity: {
		        type: Number
	      	},
		    subTotal: {
		        type: Number
	      	}
    	}
			    
	],
	createdOn: {
		type: Date,
		default: new Date()
	}
})

module.exports = mongoose.model("User", userSchema);