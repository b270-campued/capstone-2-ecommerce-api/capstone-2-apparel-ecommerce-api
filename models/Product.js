const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Name is required"]
	},
	sizeAndColor: {
		type: String,
		required: [true, "Size and color is required"]
	},
	src: {
		type: String,
		required: [true, "Image source is required"]
	},
	description: {
		type: String,
		required: [true, "Description is required"]
	},
	price: {
		type: Number,
		required: [true, "Price is required"]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	orders: [

		{
			orderId: {
				type: String
			}
		}
	]
})

module.exports = mongoose.model("Product", productSchema);