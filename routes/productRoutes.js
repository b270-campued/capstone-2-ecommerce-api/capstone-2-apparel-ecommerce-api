const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const auth = require("../auth.js")

// Route for creating a product
router.post("/", auth.verify, productController.addProduct);

// Route for retrieving all products
router.get("/all", auth.verify, productController.getAllProduct);

// Route for retrieving all active products
router.get("/", productController.getAllActiveProduct);

// Route for retrieving a specific product
router.get("/:productId",productController.getProduct);

//Route for updating a product
router.put("/:productId", auth.verify, productController.updateProductInfo);

//Route for archiving a product
router.patch("/:productId/archive", auth.verify, productController.archiveProduct);

module.exports = router;
