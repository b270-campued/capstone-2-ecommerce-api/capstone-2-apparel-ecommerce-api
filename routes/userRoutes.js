const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth.js")

// Route for checking email copy
router.post("/checkEmail", userController.checkEmailExists);

// Route for user registration
router.post("/register", userController.registerUser);

// Route for user authentication
router.post("/login", userController.loginUser);

// Route for checkout of non-admin user (Create order/Buy Now)
router.post("/:productId/checkout", auth.verify, userController.order);

// Route for retrieving user details
router.get("/details", auth.verify, userController.getProfile)

// Route for setting user as admin
router.post("/:userId/setAdmin", auth.verify, userController.setAdmin)

// Route for retrieving user orders
router.get("/orders", auth.verify, userController.getOrder)

// Route for retrieving all orders (admin)
router.get("/orders/all", auth.verify, userController.getAllOrder)

// Route for checkout of non-admin user (Create order/Buy Now)
router.post("/:productId/addToCart", auth.verify, userController.addToCart);

// Route for retrieving user cart
router.get("/cart", auth.verify, userController.getCart)
module.exports = router;
